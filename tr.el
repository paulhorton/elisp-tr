;;; tr.el ---   transliterate characters  -*- lexical-binding: t; -*-

;; Copyright (C) 2019, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20190421
;; Updated: 20190421
;; Version: 0.0
;; Keywords: transliterate, transliteration, tr, y

;;; Commentary:

;; Provide some elisp functionality similar to sed|perl y|tr.
;;
;; Also provides some utilities for working with case-tables
;;

;;
;;
;;; Change Log:

;;; Code:


; ───────────  caseTable constructors  ──────────
(defun tr/caseTable (&optional init)
  (make-char-table 'case-table init))

(defun tr/char-table/as-alist (char-table)
  "Returns an alist holding (key . val) pairs for the characters in char-table.
Wrapper around `map-char-table', but this function does not return ranges."
  (cl-assert (char-table-p char-table) t)
  (let (ret-acc)
    (map-char-table (lambda (key-or-range val)
                      (if (characterp key-or-range)
                          (push (cons key-or-range val) ret-acc)
                        (dotimes* (key (car key-or-range) (1+ (cdr key-or-range)))
                          (push (cons key val) ret-acc)
                            )))
                    char-table
                    )
    ret-acc
    ))


(defmacro tr/caseTable-assert-is (var1)
  `(or (case-table-p ,var1) (error "tr; expected %s to be a case table, but it was: %S" ',var1 ,var1)))

(defmacro tr/caseTable-assert-are (var1 var2)
    `(progn
       (or (case-table-p ,var1) (error "tr; expected %s to be a case table, but it was: %S" ',var1 ,var1))
       (or (case-table-p ,var2) (error "tr; expected %s to be a case table, but it was: %S" ',var2 ,var2))
      ))


(defun tr/caseTable/2seqs (X Y)
  "Return caseTable constructed from two sequences X and Y,
such that caseTable can map: X[i] → Y[i].
X and Y should of equal length sequences.
The characters in X should be unique."
  (or (sequencep X)  (error "tr/caseTable; expected a sequence for arg X, but got '%s'" X))
  (or (sequencep Y)  (error "tr/caseTable; expected a sequence for arg Y, but got '%s'" Y))
  (or (= (length X) (length Y))
      (error "tr/caseTable; expected equal length args, but lengths are %d, %d" (length X) (length Y)))
  (cl-loop
   with caseTable = (tr/caseTable)
   for cX elements of X
   for cY elements of Y
   if (aref caseTable cX)
   do (error "tr/caseTable; duplicate character '%s' found in X arg: %s" cX X)
   else
   do (aset caseTable cX cY)
   finally return caseTable))


;; Previous name tr/caseTable/lc-to-ups-pairs (before 2024)
(defun tr/caseTable/one-from-many (val-keys-seq)
  "Return caseTable constructed from VAL-KEYS-SEQ of pairs mapping
a key to one or more values.

Each element in VAL-KEYS-SEQ should be a 2 element sequence

  elt0: a character value
  elt1: a sequence of key characters

For example,

  (?あ \"あぁかがさざただなはばぱまやゃらわ\")

Maps each character in \"あぁかが...\" to ?あ"
  (let1 ret-caseTable (tr/caseTable)
    (seq-doseq (val-keys val-keys-seq)
      (let1 val (elt val-keys 0)
        (seq-doseq (key (elt val-keys 1))
          (aset ret-caseTable key val)
        )))
    ret-caseTable
    ))


(defun tr/caseTable/invert (caseTable)
  "Return caseTable constructed from CASETABLE, but with keys and values reversed.
If CASETABLE values are unique,
two applications of this tr/caseTable-inverted should recover the original."
  (tr/caseTable-assert-is caseTable)
  (or (case-table-p caseTable) (error "expected caseTable to be a case table but got: %S" caseTable))
  (let ((retval (tr/caseTable)))
    (map-char-table
     (lambda (key val)
       (aset retval val key))
     caseTable)
    retval))

(defun tr/caseTable/convolve (caseTable1 caseTable2)
  "Return caseTable constructed from case tables caseTable1, caseTable2
such that (aref returnedTable c) == (aref caseTable1 (aref caseTable2 c))

All keys in caseTable1 must map to values which are keys in caseTable2.
Other entries in caseTable2 are ignored."
  (tr/caseTable-assert-are caseTable1 caseTable2)
  (let ((retVal (tr/caseTable)))
    (map-char-table
     (lambda (key-or-range val)
       (if (characterp key-or-range)
           (aset  retVal  key-or-range  (aref caseTable1 val))
         (cl-loop
          for key from (car key-or-range) to (cdr key-or-range)
          do   (aset  retVal  key  (aref caseTable1 val))
          )))
     caseTable2)
    retVal))

(defun tr/caseTable/shifted (shifter to-be-shifted )
  "Return a \"shifted\" version of case table TO-BE-SHIFTED
in which both its keys and values have been replaced according to SHIFTER.

Conceptionally, if F is made with (tr/caseTable/shifted f z),
                then ∀a  F[a]→b  ⟹  f[ z[a] ]→ z[b]"
  (tr/caseTable-assert-are shifter to-be-shifted)
  (let1 ret-caseTable (tr/caseTable)
    (cl-loop
     for (key . val) in (tr/char-table/as-alist to-be-shifted)
     do (or (characterp val)  (error "value of key %S in to-be-shifted is not a character" key))
     do (or (characterp (aref shifter key)) (error "No key in shifter for character %S" key))
     do  (aset ret-caseTable
               (aref shifter key)
               (aref shifter val)))
    ret-caseTable
    ))


(defun tr/caseTable-stringified (caseTable)
  "Return string representation of CASETAB.
Keys and values printed as unicode chars."
  (tr/caseTable-assert-is caseTable)
  (let ((retval ""))
    (map-char-table
     (lambda (key val)
       (let ((keyString (if (characterp key)
                            (string key)
                              (string (car key) ?– (cdr key))))
             (valString (if (characterp val) (string val) (format "%S" val)))
             )
         (setq retval (concat retval (format "%s\t%s\n" keyString valString)))))
     caseTable)
    retval))


(defun tr/caseAlist/2seqs (X Y)
  "Return caseAlist constructed from two sequences X and Y,
such that caseAlist can map: X[i] → Y[i].
X and Y should of equal length sequences.
The characters in X should be unique."
  (or (sequencep X)  (error "tr/caseAlist/2seqs; expected a sequence for arg X, but got '%s'" X))
  (or (sequencep Y)  (error "tr/caseAlist/2seqs; expected a sequence for arg Y, but got '%s'" Y))
  (or (= (length X) (length Y))
      (error "tr/caseAlist/2seqs; expected equal length args, but lengths are %d, %d" (length X) (length Y)))
  (seq-mapn #'cons X Y))



(defun tr/replaced (char-map S)
  "Return copy of S with characters replaced according to CHAR-MAP

caseTable can obtained by calling `tr/caseTable`.

Returns string unless S is a character, in which case it returns a character."
  (cl-assert (or (sequencep S) (characterp S)) "Expected stringlike sequence or character but got %s" S)
  (cl-typecase char-map
   (case-table
    (with-case-table char-map
      (downcase S)
      ))
   (list
    ;; char-map should be an alist
    (if (characterp S)
        (map-elt char-map S S)
      (concat
       (mapcar
        (lambda (c)
          (map-elt char-map c c)
          )
        S
        ))))
   (t
    (error "unsupported type %S" char-map))
   ))


(defun tr/removed (chars S)
  "Return copy of S, but with characters in sequence CHARS removed."
  (let1  char-list  (if (stringp chars) (append chars nil) chars)
    (concat
     (seq-remove
      (lambda (c)
        (memq c char-list))
      S)
     )))


(defmacro tr/remove (chars S)
  "Remove any characters in CHARS from string S (destructively)."
  `(setq  ,S  (tr/removed ,chars ,S))
  )


(defun tr/replace (caseTable S)
  "Replace characters in sequence S according to CASETAB

Returns number of (not necessarily unique) character replacements performed."
  (cl-assert (sequencep S))
  (cl-loop
   with count = 0
   for c elements of S using (index i)
   for targetChar = (aref caseTable c)
   if targetChar
   do  (progn (cl-incf count) (aset S i targetChar))
   finally return count))


(defun tr/map-inplace (caseTable S transformFUNC)
  "Apply (setq c (funcall transformFUNC c)) to all characters c in S"
  (tr/caseTable-assert-is caseTable)
  (cl-assert (sequencep S))
  (with-case-table caseTable
    (cl-loop
     for c across-ref S
     do (setq c (funcall transformFUNC c))
     ))
  S)


(defun tr/dwcase (caseTable S)
  "Downcase characters in sequence S according to CASETAB"
  (tr/map-inplace caseTable S #'downcase))

(defun tr/upcase (caseTable S)
  "Downcase characters in sequence S according to CASETAB"
  (tr/map-inplace caseTable S #'upcase))


;;(defun tr/string-pair-cons←char-map (char-map
;;  "Return cons cell holding the mappings in CHAR-MAP as
;;(aref (car cons-cell) i) -->  (aref (cdr cons-cell) i)"
;;  (cl-typecase char-map
;;    (case-table
;;    (sort


(defun tr/string-pair←char-map-keys (char-map)
  "CHAR-MAP keys as a string"
  (let ((key-LIST) (val-LIST))
  (cl-typecase char-map
    (case-table
       (map-char-table
        (lambda (key val)
          (if (consp key)
              (cl-loop
               for i from (car key) to (cdr key)
               do
               (push i key-LIST)
               (push val val-LIST)
               )
            (push key key-LIST)
            ))
        char-map
        )
     (concat key-LIST)
     )
    (t (error "hash not yet implemented")
    ))))


; ───────────  non-caseTable utilities  ──────────
(cl-defun tr/string-sorted (s &optional (predicate #'<))
  "Return string with containing characters of string S,
sorted according to PREDICATE."
  (concat
   (sort (mapcar 'identity s) predicate)))



(provide 'tr)

;;; tr.el ends here
