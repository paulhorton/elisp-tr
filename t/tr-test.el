;;; tr-test.el ---   -*- lexical-binding: t -*-

;; Copyright (C) 2021, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20210212
;; Updated: 20210212
;; Version: 0.0
;; Keywords: 

;;; Commentary:

;;; Change Log:

;;; Code:


(ert-deftest tr-test--caseAlist/2seqs ()
  (should
   (equal
    '((?a . ?A) (?b . ?B) (?c . ?C))
    (tr/caseAlist/2seqs "abc" "ABC")
    )))

(ert-deftest tr-test--removed ()
  (should
   (equal
    "ctfrsl"
    (tr/removed "aeiou" "catforsale")
    )))
      

;;; tr-test.el ends here


