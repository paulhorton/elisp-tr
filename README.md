# elisp-tr

Some functionality similar to tr in Perl or y in sed.

Functions work in two steps

1. Obtain an MAPPING object to map X→Y
2. Call replacement functions using that MAPPING and a sequence to be transformed

For example to obtain a MAPPING object mapping digit characters onto their
roman numeral equivilants, {tr/123456789/ⅠⅡⅢⅣⅤⅥⅦⅧⅨ/} in sed/perl-like notation)

One may use any of the following:

    (setq to-roman (tr/mapping "123456789" "ⅠⅡⅢⅣⅤⅥⅦⅧⅨ"))

    (setq to-roman (tr/mapping "123456789" [?Ⅰ ?Ⅱ ?Ⅲ ?Ⅳ ?Ⅴ ?Ⅵ ?Ⅶ ?Ⅷ ?Ⅸ]))

Then to-roman can be used to make replacements

    (setq string-to-try  "1: platinum, 2: gold, 3: silver, 4: bronze, 5: copper")

    (tr/replaced to-roman string-to-try)"

Returns string "Ⅰ: platinum, Ⅱ: gold, Ⅲ: silver, Ⅳ: bronze, Ⅴ: copper"

While

    (tr/replace to-roman string-to-try)

Returns the number of replacements (5 in this case) and sets
string-to-try to "Ⅰ: platinum, Ⅱ: gold, Ⅲ: silver, Ⅳ: bronze, Ⅴ: copper".


## Provides Functions

    (tr/mapping  X Y)        Returns an X→Y mapping object suitable for use with the other tr functions
    (tr/replaced MAPPING S)  Returns copy of sequence S with characters replaced accoring to MAPPING
    (tr/replace  MAPPING S)  Replaces characters in S according to MAPPING; and returns count of replacements.


## Author

Paul Horton


## License

GNU Public License
